"""Setup VNC for a novnc demo

Instructions:
Wait for the experiment to start, click on the VNC option in the node
context menu.
"""

#
# This is the install/setup/start script.
#
STARTVNC = "/bin/bash /local/repository/startvnc.sh"

# Import the Portal object.
import geni.portal as portal
# Import the ProtoGENI library.
import geni.rspec.pg as rspec
# Emulab specific extensions.
import geni.rspec.emulab as emulab

# Create a Request object to start building the RSpec.
request = portal.context.makeRequestRSpec()

#
# Declare that you will be starting X11 VNC on (some of) your nodes.
# You must have this line for X11 VNC to work.
#
request.initVNC()
 
# Add a raw PC to the request.
node = request.RawPC("node")

#
# Tell the system we are going to start X11 VNC on this node.
# Since the point of this profile is to demonstrate how, we
# indicate that we are starting it. Normally, this call would
# add whatever is needed to install and start VNC.
#
node.startVNC(nostart=True)

#
# Start X11 VNC from the repo.
# For now the port is 5901, at some point we may allow setting the port.
#
node.addService(rspec.Execute(shell="sh", command=STARTVNC))
        
portal.context.printRequestRSpec()

